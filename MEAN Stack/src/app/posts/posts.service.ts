import {Post} from './post.module';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import { environment } from '../../environments/environment';
import {BACKSLASH} from '@angular/cdk/keycodes';

const BACKEND_URL = environment.apiUrl + '/posts/';

@Injectable({providedIn: 'root'})
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<{posts: Post[], postCount: number}>();

  constructor(private http: HttpClient, private router: Router) {}

  getPosts(postsPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`;
    this.http
      .get<{message: string, posts: any, maxPosts: number}>(BACKEND_URL + queryParams)
      .pipe(map((postData) => {
        return {
          posts: postData.posts.map(post => {
            return{
              title: post.title,
              content: post.content,
              id: post._id,
              imagePath: post.imagePath,
              creator: post.creator
            };
          }),
          maxPosts: postData.maxPosts
        };
      }))
      .subscribe((mappedPosts) => {
        this.posts = [...mappedPosts.posts];
        this.postsUpdated
          .next({
            posts: [...this.posts], postCount: mappedPosts.maxPosts
          });
      });
  }

  getPostsUpdatedListener(){
    return this.postsUpdated.asObservable();
  }

  addPost(title: string, content: string, image: File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);
    this.http.post<{message: string, post: Post}>(BACKEND_URL, postData)
      .subscribe((response) => {
        // const post: Post = {...response.post};
        // this.posts.push(post);
        // this.postsUpdated.next([...this.posts]);
        this.router.navigate(['/']);
      });
  }

  updatePost(postId: string, title: string, content: string, image: File | string) {
    let postData: Post | FormData;
    if (typeof (image) === 'object'){
      postData = new FormData();
      postData.append('id', postId);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    }else{
      postData = {id: postId, title: title, content: content, imagePath: image, creator: null};
    }
    this.http.put<{message: string, imagePath: string}>(BACKEND_URL + postId, postData)
      .subscribe((response) => {
        // const updatedPosts = [...this.posts];
        // const oldPostIndex = this.posts.findIndex(p => p.id === postId);
        // const post: Post = {id: postId, title: title, content: content, imagePath: response.imagePath};
        // updatedPosts[oldPostIndex] = post;
        // this.posts = updatedPosts;
        // this.postsUpdated.next([...this.posts]);
        this.router.navigate(['/']);
      });
  }

  getPost(postId: string){
    return this.http.get<{_id: string, title: string, content: string, imagePath: string, creator: string}>(`${BACKEND_URL}${postId}`);
  }

  deletePost(postId: string){
    // this.http.delete('http://localhost:3000/api/posts/' + postId)
    //   .subscribe(() => {
    //     const updatedPosts = this.posts.filter(post => post.id !== postId);
    //     this.posts = updatedPosts;
    //     this.postsUpdated.next([...this.posts]);
    //   });
    return this.http.delete(BACKEND_URL + postId);
  }
}
