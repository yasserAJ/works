const express = require("express");

const checkAuth = require('../middleware/check-auth');
const PostsController = require('../controllers/posts');
const extractFile = require('../middleware/file_extractor');

const router = express.Router();

router.post('', checkAuth, extractFile, PostsController.createPost);

router.get('', PostsController.getPosts);

router.get('/:id', PostsController.getPost);

router.delete("/:id", checkAuth, PostsController.deletePost);

router.put("/:id", checkAuth, extractFile, PostsController.updatePost);

module.exports = router;
