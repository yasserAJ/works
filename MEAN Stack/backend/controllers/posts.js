const Post = require('../models/post');

exports.createPost = (req, res, next) => {
  const url = req.protocol + '://' + req.get('host');
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + '/images/' + req.file.filename,
    creator: req.userData.userId
  });
  post.save()
    .then(createdPost => {
      res.status(201).json({
        message: 'Post added successfully!',
        post: {
          ...createdPost,
          id: createdPost._id,
        }
      });
    })
    .catch(error => {
      return res.status(500).json({message: 'Creating a post failed!'})
    })
};

exports.getPosts = (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  let fetchedPosts;
  const postQuery = Post.find();
  if (pageSize && currentPage) {
    postQuery
      .skip(pageSize * (currentPage - 1))
      .limit(pageSize);
  }
  postQuery
    .then((documents) => {
      fetchedPosts = documents;
      return Post.countDocuments();
    })
    .then(count => {
      res.status(200).json({
        message: 'Posts retrieved successfuly!',
        posts: fetchedPosts,
        maxPosts: count
      });
    })
    .catch(error => {
      return res.status(500).json({message: 'Failed to retrieve the posts!'});
    })
};

exports.getPost = (req, res, next) => {
  Post.findById(req.params.id)
    .then(post => {
      if (post) {
        return res.status(200).json(post);
      } else {
        return res.status(404).json({message: 'Post not found!'});
      }
    })
    .catch(error => {
      return res.status(500).json({message: 'Failed to retrieve the post!'});
    })
};

exports.deletePost = (req, res, next) => {
  Post.deleteOne({_id: req.params.id, creator: req.userData.userId})
    .then(result => {
      if (result.n > 0) {
        res.status(200).json({message: "Post Deleted!"});
      } else {
        res.status(401).json({message: "Unauthorized!"});
      }
    })
    .catch(error => {
      return res.status(500).json({message: 'Post deletion failed!'})
    })
};

exports.updatePost = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + '://' + req.get('host');
    imagePath = url + '/images/' + req.file.filename;
  }
  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId
  });
  Post.updateOne(
    {_id: req.params.id, creator: req.userData.userId}, post)
    .then(result => {
      if (result.n > 0) {
        res.status(201).json({message: "Update successful!", imagePath: imagePath});
      } else {
        res.status(401).json({message: "Unauthorized!"});
      }
    })
    .catch(error => {
      return res.status(500).json({message: 'Post update failed!'});
    })
};
