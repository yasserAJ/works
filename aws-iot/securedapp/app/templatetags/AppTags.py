from django import template

register = template.Library()


# Converts from celsius to fahrenheit
@register.filter(name='CtoF')
def cut(value):
    return (value * 9) / 5 + 32
