var tour = new Tour({
  steps: [
  {
    element: "#custom-radios",
    title: "System state controls",
    content: "Use the <span style='color: #00cc00'>button</span> to turn on the system, use the <span style='color: #ff0000'> button </span> to " +
    "turn it off instead. The <span style='color: #ffda00;'>button</span> is used to make the system dependent on motion."
  }
]});

// Initialize the tour
tour.init();

// Start the tour
tour.start();