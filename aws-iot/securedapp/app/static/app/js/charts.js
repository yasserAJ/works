function updateChart(response){
            var received = response.chartsData;
            var dataTemp = [];
            var dataLum = [];
            var dataPres = [];
            dataTemp.push([received[0].timestamp*1000, received[0].temperature]);
            dataLum.push([received[0].timestamp*1000, received[0].brightness]);
            dataPres.push([received[0].timestamp*1000, received[0].presence]);
            var presenceAnt = received[0].presence;
            for(i=0; i<received.length; i++){
                dataTemp.push([received[i].timestamp*1000, received[i].temperature]);
                dataLum.push([received[i].timestamp*1000, received[i].brightness]);
                if(presenceAnt!=received[i].presence){
                    dataPres.push([received[i].timestamp*1000-1, presenceAnt]);
                }
                dataPres.push([received[i].timestamp*1000, received[i].presence]);
                presenceAnt = received[i].presence;

            }
            Highcharts.chart('containerCharts', {
                chart: {
                    zoomType: 'xy'
                },

                title: {
                    text: 'Reported data'
                },

                xAxis: {
                    title: {
                        text: 'Time'
                    },
                    type : 'datetime'
                },

                yAxis: [{
                    title: {
                        text: 'Temperature'
                    }
                },	{
                    title: {
                        text: 'Brightness'
                    },
                    opposite: true
                },	{
                    title: {
                        text: 'Presence'
                    },
                    categories: ['No', 'Yes'],
                    opposite: true
                }],

                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                },
                series: [{
                    name: 'Temperature ºC',
                    data: dataTemp
                }, {
                    name: 'Brightness',
                    data: dataLum,
                    yAxis: 1
                },	{
                    name: 'Presence',
                    data: dataPres,
                    yAxis: 2
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
        }