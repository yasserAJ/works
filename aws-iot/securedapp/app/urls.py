from django.conf.urls import url
from . import views
app_name = 'app'
urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^home/$', views.home, name='home'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^signout$', views.signout, name='logout'),
    url(r'^temperature$', views.setTemp, name='temperature'),
    url(r'^light$', views.setLight, name='light'),
    url(r'^refresh$', views.refresh, name='refresh'),
    url(r'^mode$', views.setMode, name='mode')
]