import json
import requests

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth import login, logout
from django.contrib.auth import authenticate
from django.contrib import messages
from django.urls import reverse
from securedapp.settings import client
from app.models import Reported
import simplejson


# Create your views here.
def email_validation(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def signin(request):
    if request.method == 'GET':
        return render(request, 'app/login.html', {})
    elif request.method == 'POST':
        # aqui se recupera el email
        email = request.POST.get('email')
        # validate if email is a valid email
        if not email_validation(email):
            messages.add_message(request, messages.ERROR, 'Email is not valid')
            return render(request, 'app/login.html')
        # Aqui se recupera el password
        password = request.POST.get('password')
        # we authenticate the user, if table User exists in database user won't be None
        user = authenticate(username=email, password=password)
        if user is None:
            messages.add_message(request, messages.ERROR, 'Wrong credentials')
            return render(request, 'app/login.html')
        else:
            # Login the user
            login(request, user)
            # user is authenticated then we redirect to a welcome page
            return HttpResponseRedirect(reverse('app:home'))
    return render(request, 'app/login.html', {})


def signup(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('pass')
        repassword = request.POST.get('repass')
        # Validation of the passwords and the email
        if not password == repassword:
            messages.add_message(request, messages.ERROR, 'Passwords do not match!')
            return render(request, 'app/signup.html')
        if not email_validation(email):
            messages.add_message(request, messages.ERROR, 'Email is not valid')
            return render(request, 'app/signup.html')
        # We check if the email doesn't exist already
        if User.objects.filter(username=email).exists():
            messages.add_message(request, messages.ERROR, 'Email already exists')
            return render(request, 'app/signup.html')
        # Crete the new user in the data base
        new_user = User.objects.create_user(email, email, password)
        new_user.save()
        # authenticating the new user
        logged_user = authenticate(username=email, password=password)
        if logged_user is not None:
            # Login the new user to redirect him stright to the home page
            login(request, logged_user)
            # we redirect the user to the home page after the success of the login
            return HttpResponseRedirect(reverse('app:home'))
        else:
            messages.add_message(request, messages.ERROR, 'Sign up failed :( , please try again.')
    elif request.method == 'GET':
        return render(request, 'app/signup.html', {})
    return render(request, 'app/signup.html', {})


def saveReported(payload):
    reportedMetadata = payload['metadata']['reported']
    reportedData = payload['state']['reported']
    presence = 1 if reportedData['presencia'] is False else 0
    timestamp = reportedMetadata['temperatura']['timestamp']
    first = Reported(temperature=round(reportedData['temperatura'], 2),
                     brightness=reportedData['luminosidad'],
                     presence=presence,
                     timestamp=timestamp)
    first.save()


def updateDatabase(payload):
    try:
        last = Reported.objects.latest()
        lastTime = last.timestamp
        reportedTempTime = payload['metadata']['reported']['temperatura']['timestamp']
        reportedLightTime = payload['metadata']['reported']['luminosidad']['timestamp']
        reportedPresenceTime = payload['metadata']['reported']['luminosidad']['timestamp']
        if lastTime < reportedTempTime or lastTime < reportedLightTime or lastTime < reportedPresenceTime:
            saveReported(payload)
        else:
            pass
    except Reported.DoesNotExist:
        saveReported(payload)


def chartData():
    data = Reported.objects.values('temperature', 'brightness', 'presence', 'timestamp')[:20]
    data = list(data)
    data.sort(key=lambda x: x['timestamp'], reverse=False)
    return data


@login_required(login_url='/signin/')
def home(request):
    c = {}
    if request.method == 'GET':
        # Get Málaga's current temperature
        cityTemp = get_malaga_temperaure()
        c.update({'minTemp': cityTemp})
        # c.update({'minTemp': 10})
        # Get the data from the thing shadow
        response = client.get_thing_shadow(
            thingName='thing2'
        )
        payload = json.load(response['payload'])
        # Get the metada so we can check if there was any change in the reported data and then store it
        updateDatabase(payload)
        charts = chartData()
        json_list = simplejson.dumps({'chartsData': list(charts)})
        c.update({'state': payload['state'], 'charts': json_list})
        return render(request, 'app/index.html', c)
    else:
        # Si alguien manda un post a esta página, tiene malas intenciones,
        # le hago logout y le devuelvo a login
        if request.user.is_authenticated:
            logout(request)
        return render(request, 'app/login.html', c)


@login_required(login_url='/signin/')
def signout(request):
    logout(request)
    return HttpResponseRedirect(reverse('app:signin'))


@login_required(login_url='/signin/')
def setTemp(request):
    desiredTemp = request.POST.get('level', None)
    if desiredTemp is None:
        response_data = {'code': 400, 'text': 'Bad request'}
    else:
        data = {'state': {'desired': {'temperaturaObjetivo': desiredTemp}}}
        response = client.update_thing_shadow(
            thingName='thing2',
            payload=json.dumps(data)
        )
        res_json = json.load(response['payload'])
        print(res_json)
        response_data = {'code': 201}
    return HttpResponse(json.dumps(response_data))


@login_required(login_url='/signin/')
def setLight(request):
    desiredLight = request.POST.get('level', None)
    if desiredLight is None:
        response_data = {'code': 400, 'text': 'Bad request'}
    else:
        data = {'state': {'desired': {'luminosidadObjetivo': desiredLight}}}
        response = client.update_thing_shadow(
            thingName='thing2',
            payload=json.dumps(data)
        )
        res_json = json.load(response['payload'])
        print(res_json)
        response_data = {'code': 201}
    return HttpResponse(json.dumps(response_data))


@login_required(login_url='/signin/')
def setMode(request):
    mode = request.POST.get('mode', None)
    if mode is None:
        response_data = {'code': 400, 'text': 'Bad request'}
    else:
        if mode not in {'presencia', 'apagado', 'encendido'}:
            print('Values are not valid')
            response_data = {'code': 400, 'text': 'Invalid value'}
        else:
            data = {'state': {'desired': {'modoFuncionamiento': mode}}}
            response = client.update_thing_shadow(
                thingName='thing2',
                payload=json.dumps(data)
            )
            res_json = json.load(response['payload'])
            print(res_json)
            response_data = {'code': 201}
    return HttpResponse(json.dumps(response_data))


@login_required(login_url='/signin/')
def refresh(request):
    response_data = {}
    # Get weather in Málaga (id 2514256 in the openweathermap API)
    cityTemp = get_malaga_temperaure()
    response_data.update({'minTemp': cityTemp})
    # response_data.update({'minTemp': 10})
    # Get the data from the thing shadow
    response = client.get_thing_shadow(
        thingName='thing2'
    )
    payload = json.load(response['payload'])
    # updateDatabase(payload)
    charts = chartData()
    response_data.update({'code': 200, 'state': payload['state']['reported'],
                          'chartsData': charts})
    return HttpResponse(json.dumps(response_data))


def get_malaga_temperaure():
    # Get weather in Málaga (id 2514256 in the openweathermap API)
    wr = requests.get(
        'http://api.openweathermap.org/data/2.5/weather?id=2514256&units=metric&appid=83620344f156f167fed1e948dbc4171d')
    return wr.json()['main']['temp']
