from django.db import models


# Create your models here.
class Reported(models.Model):
    temperature = models.FloatField(blank=True, null=True)
    brightness = models.IntegerField(blank=True, null=True)
    presence = models.SmallIntegerField(blank=True, null=True)
    timestamp = models.IntegerField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-timestamp',)
        get_latest_by = 'timestamp'
