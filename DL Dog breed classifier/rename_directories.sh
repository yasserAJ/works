#!/bin/sh

directories="*/"
regex="n[0-9]+-{1}([0-9a-zA-Z_-]+/)"
for old_directory in $directories
do
    if [[ $old_directory =~ $regex ]]
    then
        new_directory="${BASH_REMATCH[1]}"
        #new_directory=$(echo $old_directory | pcregrep -o1 $regex)
        echo "${new_directory}"
        mv $old_directory/* $new_directory
    else
        echo "$d doesn't match" >&2
    fi
done
