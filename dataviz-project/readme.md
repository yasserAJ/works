The dataset used for the visual analysis is pulled from http://followthehashtag.com/datasets/spain_portugal/.
To use the application install:
* Python >3.x
* Django

For an easy setup use the requierements.txt along with pip:
`pip install -i requierements.txt`

To execute the application and open it on the browser:
`python manage.py runserver`
