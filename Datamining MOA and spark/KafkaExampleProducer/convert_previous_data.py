import sys
from datetime import datetime

import numpy as np
import pandas as pd
from confluent_kafka import Producer

import settings

D_TYPE = {
    'poiID': np.int32,
    'latitude': np.float32,
    'longitude': np.float32,
    'altitud': np.float32,
    'capacidad': np.int32,
    'capacidad_discapacitados': np.int32,
    'libres': np.int32,
    'libres_discapacitados': np.int32,
    'nivelocupacion_naranja': np.int32,
    'nivelocupacion_rojo': np.int32,
    'smassa_sector_sare': np.int32
}

week = ['Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday']

# Topic to write
TOPIC = settings.CLOUDKARAFKA_TOPICS.split(',')[0]

monday = datetime(year=2018, month=12, day=10)
tuesday = datetime(year=2018, month=12, day=11)
wednesday = datetime(year=2018, month=12, day=12)
thursday = datetime(year=2018, month=12, day=13)
friday = datetime(year=2018, month=12, day=14)
saturday = datetime(year=2018, month=12, day=15)
sunday = datetime(year=2018, month=12, day=16)

DATE_DAY = "fechahora_ultima_actualizacion_dia"
DATE_HOUR = "fechahora_ultima_actualizacion_hora"


def change_date(data):
    if data[DATE_DAY] == week[0]:
        date = sunday
    elif data[DATE_DAY] == week[1]:
        date = monday
    elif data[DATE_DAY] == week[2]:
        date = tuesday
    elif data[DATE_DAY] == week[3]:
        date = wednesday
    elif data[DATE_DAY] == week[4]:
        date = thursday
    elif data[DATE_DAY] == week[5]:
        date = friday
    else:
        date = saturday

    hour = int(data[DATE_HOUR])

    return str(date.replace(hour=hour))


def configure_producer() -> Producer:
    # Consumer configuration
    # See https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
    conf = {
        'bootstrap.servers': settings.CLOUDKARAFKA_BROKERS,
        'session.timeout.ms': 6000,
        'default.topic.config': {'auto.offset.reset': 'smallest'},
        'security.protocol': 'SASL_SSL',
        'sasl.mechanisms': 'SCRAM-SHA-256',
        'sasl.username': settings.CLOUDKARAFKA_USERNAME,
        'sasl.password': settings.CLOUDKARAFKA_PASSWORD,
        'ssl.ca.location': './config/cloudkarafka_ca'
    }

    return Producer(**conf)


def delivery_callback(error, message):
    if error:
        sys.stderr.write('%% Message failed delivery: {}\n'.format(error))
    else:
        sys.stderr.write('%% Message delivered to {} [{}]\n'.format(message.topic(), message.partition()))


parking = pd.read_csv("old.csv", sep=",", dtype=np.str_)
parking["latitude"] = pd.to_numeric(parking["latitude"], errors='coerce', downcast='float')
parking["longitude"] = pd.to_numeric(parking["longitude"], errors='coerce', downcast='float')
parking["altitud"] = pd.to_numeric(parking["altitud"], errors='coerce', downcast='float')
parking["capacidad"] = pd.to_numeric(parking["capacidad"], errors='coerce', downcast="integer")
parking["capacidad_discapacitados"] = pd.to_numeric(parking["capacidad_discapacitados"], errors='coerce',
                                                    downcast='integer')
parking["libres"] = pd.to_numeric(parking["libres"], errors='coerce', downcast='integer')
parking["libres_discapacitados"] = pd.to_numeric(parking["libres_discapacitados"], errors='coerce',
                                                 downcast='integer')
parking["nivelocupacion_naranja"] = pd.to_numeric(parking["nivelocupacion_naranja"], errors='coerce',
                                                  downcast='integer')
parking["nivelocupacion_rojo"] = pd.to_numeric(parking["nivelocupacion_rojo"], errors='coerce', downcast='integer')
parking["smassa_sector_sare"] = pd.to_numeric(parking["smassa_sector_sare"], errors='coerce', downcast='integer')

# Filter wrong data
parking = parking[parking["libres"] >= 0]

parking["fechahora_ultima_actualizacion"] = parking[[DATE_DAY, DATE_HOUR]].apply(change_date, axis=1)

# Filter data
parking = parking[['altitud', 'latitude', 'longitude', 'libres', 'poiID', 'fechahora_ultima_actualizacion', 'nombre']]

# Configure producer
producer = configure_producer()

step = 1000
start_min = 0
start_max = step
rows, cols = parking.shape

while start_max < rows:
    parkingSmall = parking.iloc[start_min:start_max]

    try:
        print("Produce data...")
        producer.produce(topic=TOPIC, value=parkingSmall.to_json(orient='records'), callback=delivery_callback)
    except BufferError as error:
        sys.stderr.write(
            '%% Local producer queue is full ({} messages awaiting delivery): try again\n'.format(len(producer)))

    # Flush producer
    producer.flush()

    start_min = start_max
    start_max = min(start_max + step, rows)
