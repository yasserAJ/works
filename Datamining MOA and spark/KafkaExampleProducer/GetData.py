import os
import sys
import urllib.request
from time import time, sleep

import numpy as np
import pandas as pd
from confluent_kafka import Producer

import settings

# url from file
URL = "https://datosabiertos.malaga.eu/recursos/aparcamientos/ocupappublicosmun/ocupappublicosmun.csv"

# Streaming dir
STREAMING_DIR = "./data/{}.csv"

# Each minute
SECONDS_TO_RECOVER = 0.5 * 60

# Conversion data
D_TYPE = {
    'poiID': np.int32,
    'latitude': np.float32,
    'longitude': np.float32,
    'altitud': np.float32,
    'capacidad': np.int32,
    'capacidad_discapacitados': np.int32,
    'libres': np.int32,
    'libres_discapacitados': np.int32,
    'nivelocupacion_naranja': np.int32,
    'nivelocupacion_rojo': np.int32,
    'smassa_sector_sare': np.int32
}

# Topic to write
TOPIC = settings.CLOUDKARAFKA_TOPICS.split(',')[0]


def get_file() -> str:
    """
    Recover file from global url.
    :return:
    """

    # Make timestamp
    time_stamp = str(time())

    # Set new file
    path = STREAMING_DIR.format(time_stamp)

    # Get and save file in STREAMING_DIR
    urllib.request.urlretrieve(URL, path)

    return path


def prepare_data(path: str):
    parking = pd.read_csv(path, sep=",", dtype=np.str_)
    parking["latitude"] = pd.to_numeric(parking["latitude"], errors='coerce', downcast='float')
    parking["longitude"] = pd.to_numeric(parking["longitude"], errors='coerce', downcast='float')
    parking["altitud"] = pd.to_numeric(parking["altitud"], errors='coerce', downcast='float')
    parking["capacidad"] = pd.to_numeric(parking["capacidad"], errors='coerce', downcast="integer")
    parking["capacidad_discapacitados"] = pd.to_numeric(parking["capacidad_discapacitados"], errors='coerce',
                                                        downcast='integer')
    parking["libres"] = pd.to_numeric(parking["libres"], errors='coerce', downcast='integer')
    parking["libres_discapacitados"] = pd.to_numeric(parking["libres_discapacitados"], errors='coerce',
                                                     downcast='integer')
    parking["nivelocupacion_naranja"] = pd.to_numeric(parking["nivelocupacion_naranja"], errors='coerce',
                                                      downcast='integer')
    parking["nivelocupacion_rojo"] = pd.to_numeric(parking["nivelocupacion_rojo"], errors='coerce', downcast='integer')
    parking["smassa_sector_sare"] = pd.to_numeric(parking["smassa_sector_sare"], errors='coerce', downcast='integer')

    # Filter wrong data
    parking = parking[parking["libres"] >= 0]

    return parking


def configure_producer() -> Producer:
    # Consumer configuration
    # See https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
    conf = {
        'bootstrap.servers': settings.CLOUDKARAFKA_BROKERS,
        'session.timeout.ms': 6000,
        'default.topic.config': {'auto.offset.reset': 'smallest'},
        'security.protocol': 'SASL_SSL',
        'sasl.mechanisms': 'SCRAM-SHA-256',
        'sasl.username': settings.CLOUDKARAFKA_USERNAME,
        'sasl.password': settings.CLOUDKARAFKA_PASSWORD,
        'ssl.ca.location': './config/cloudkarafka_ca'
    }

    return Producer(**conf)


def delivery_callback(error, message):
    if error:
        sys.stderr.write('%% Message failed delivery: {}\n'.format(error))
    else:
        sys.stderr.write('%% Message delivered to {} [{}]\n'.format(message.topic(), message.partition()))


def main():
    # Configure producer
    producer = configure_producer()

    # Run infinite loop
    while True:
        # Get file from url
        print("Getting file...")
        path = get_file()

        # Pre-processing data
        print("Processing data...")

        try:
            data = prepare_data(path=path)
        except Exception:
            print("Not valid data.")
            # Omit this data
            continue

        # Filter data
        data = data[['altitud', 'latitude', 'longitude', 'libres', 'poiID', 'fechahora_ultima_actualizacion', 'nombre']]

        try:
            print("Produce data...")
            producer.produce(topic=TOPIC, value=data.to_json(orient='records'), callback=delivery_callback)
        except BufferError as error:
            sys.stderr.write(
                '%% Local producer queue is full ({} messages awaiting delivery): try again\n'.format(len(producer)))

        # Delete processed file
        print("Deleting file...")
        if os.path.exists(path=path):
            os.remove(path=path)

        # Flush producer
        producer.flush()

        print("Waiting...")
        sleep(SECONDS_TO_RECOVER)


if __name__ == '__main__':
    main()
